/*
 * This is a template for how a game will look. We should design the API around
 * this.
 *
 * GWindow is a Gobie Window.
 * GImage is a Gobie Image.
 */

class GameWindow : public GWindow
{
	GImage bg; // This might or might not need to be a scoped_ptr

public:
	GameWindow() : Window(640, 480, false) // width, height, fullscreen
	{
		setCaption("Gobie Game");
		bg.load(graphics(), "bg.png");
	}

	void update()
	{
	}

	void draw() const
	{
		bg.draw(0, 0);
	}
};

int main()
{
	GameWindow window;
	window.show();
}

