#include <QHash>
#include <QString>
#include <QTime>

template <class T>
struct QCacheEntry
{
	QTime deletedAt;
	unsigned refCount;
	T t;
};

template <class T>
class QCache
{
	QCashe();
	~QCache();

	bool contains(const QString& key) const;
	T& get(const QString& key);
	void insert(const QString& key, const T& value);
	int remove(const QString& key);

	void purge(QTime& olderThan);

private:
	QHash<QString, QCacheEntry<T> > pool;
};

