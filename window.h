class Window
{
public:
	Audio& audio();
	Graphics& graphics();

private:
	Audio a;
	Graphics g;
};

