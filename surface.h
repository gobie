#include <SDL.h>

class Surface
{
public:
	Surface();
	~Surface();

	int width();
	int height();
	void draw(Surface& target, int x, int y);

private:
	SDL_Surface* s;
};

