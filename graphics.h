#include "qcache.h"
#include "surface.h"

class Graphics
{
public:
	Surface* getSurface(QString name);
	void purge(); // free all not-currently-used surfaces

	int getWidth();
	int getHeight();

private:
	QCache<Surface> surfs;
};

